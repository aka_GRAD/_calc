object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = '_calc'
  ClientHeight = 150
  ClientWidth = 195
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 8
    Top = 89
    Width = 179
    Height = 32
    Caption = #1042#1099#1095#1080#1089#1083#1080#1090#1100
    OnClick = SpeedButton1Click
  end
  object Label1: TLabel
    Left = 8
    Top = 129
    Width = 39
    Height = 13
    Caption = #1054#1090#1074#1077#1090' :'
  end
  object Label2: TLabel
    Left = 53
    Top = 129
    Width = 3
    Height = 13
  end
  object sl1_e: TEdit
    Left = 10
    Top = 8
    Width = 177
    Height = 21
    NumbersOnly = True
    TabOrder = 0
    Text = '0'
  end
  object sl2_e: TEdit
    Left = 8
    Top = 62
    Width = 179
    Height = 21
    NumbersOnly = True
    TabOrder = 1
    Text = '0'
  end
  object _zn: TComboBox
    Left = 8
    Top = 35
    Width = 179
    Height = 21
    ItemIndex = 0
    TabOrder = 2
    Text = '+'
    Items.Strings = (
      '+'
      '-'
      '/'
      '*')
  end
end
