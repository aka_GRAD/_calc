unit calc_unit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    sl1_e: TEdit;
    sl2_e: TEdit;
    _zn: TComboBox;
    SpeedButton1: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
 if (Trim(sl1_e.Text)='') or (Trim(sl2_e.Text)='') then
  begin
   Application.MessageBox('��� ��������� ������', '������', MB_ICONERROR);
   exit;
  end;

 case _zn.ItemIndex of
   0:
     label2.Caption:= FloatToStr(StrToFloat(sl1_e.Text)+StrToFloat(sl2_e.Text));
   1:
     label2.Caption:= FloatToStr(StrToFloat(sl1_e.Text)-StrToFloat(sl2_e.Text));
   2:
     label2.Caption:= FloatToStr(StrToFloat(sl1_e.Text)/StrToFloat(sl2_e.Text));
   3:
     label2.Caption:= FloatToStr(StrToFloat(sl1_e.Text)*StrToFloat(sl2_e.Text));
 end;

end;

end.
